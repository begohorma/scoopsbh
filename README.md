# Práctica MBaaS iOS Swift Firebase (ScoopsBH) #


### Notas ###

Debido a la falta de tiempo:

* Me he centrado en la funcionalidad y no me he preocupado por el aspecto de la aplicación
* De los datos del post no he incluido la localización
* Falta guardar la fecha de publicación para que los post de usuario lector estén ordenados por esa fecha
* No se muestra el autor porque he guardado el id de la base de datos. Queda pendiente cambiarlo para que muestre email

## Proceso de publicación:

* Debido a que no se termino de explicar las tareas en backend en firebase he puesto un botón en la ventana de los post del autor que permite pasar todos los sus post listos para publicar a publicados. En esa ventana también he añadido unos botones que permiten mostrar los post del autor según su estado:
 - Play: post publicados
 - Pause: post listos para publicar
 - X : post que no están listos para publicar



## Push Notification
 - No está incluido porque aún no tengo usuario desarrollador de pago de apple.


## Estructura de BD y Reglas

He decidido usar tres ramas diferentes:

* posts: para guardar todos los post. Para la parte de lector
* author-post: para guardar los post de cada autor. Para la parte de autor
* post-rates: para guardar las valores de cada post y poder calcular el valor medio

![Captura de pantalla 2017-04-09 a las 22.37.28.png](https://bitbucket.org/repo/AgGB9AR/images/3482391731-Captura%20de%20pantalla%202017-04-09%20a%20las%2022.37.28.png)

![Captura de pantalla 2017-04-09 a las 22.37.54.png](https://bitbucket.org/repo/AgGB9AR/images/2309227832-Captura%20de%20pantalla%202017-04-09%20a%20las%2022.37.54.png)

![Captura de pantalla 2017-04-09 a las 22.37.41.png](https://bitbucket.org/repo/AgGB9AR/images/2897580558-Captura%20de%20pantalla%202017-04-09%20a%20las%2022.37.41.png)

![Captura de pantalla 2017-04-09 a las 22.38.05.png](https://bitbucket.org/repo/AgGB9AR/images/1275829115-Captura%20de%20pantalla%202017-04-09%20a%20las%2022.38.05.png)

- Reglas

![Captura de pantalla 2017-04-09 a las 22.38.14.png](https://bitbucket.org/repo/AgGB9AR/images/1357758272-Captura%20de%20pantalla%202017-04-09%20a%20las%2022.38.14.png)

## Reglas Storage

![Captura de pantalla 2017-04-09 a las 22.38.39.png](https://bitbucket.org/repo/AgGB9AR/images/1551856910-Captura%20de%20pantalla%202017-04-09%20a%20las%2022.38.39.png)