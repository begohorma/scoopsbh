//
//  PostRate.swift
//  ScoopsBH
//
//  Created by Begoña Hormaechea on 9/4/17.
//  Copyright © 2017 COM. All rights reserved.
//

import Foundation
import Firebase

class PostRate: NSObject{
    
    
    var postKey: String
//    var rate: Double
    var rate: String

    var refInCloud: FIRDatabaseReference?

//    init(postKey:String, rate:Double) {
    init(postKey:String, rate:String) {
        self.postKey = postKey
        self.rate = rate
        self.refInCloud = nil
    }
    
    //init basado en el snapshot
    init?(snap: FIRDataSnapshot){
        guard let dict = snap.value as? [String:Any] else{ return nil}
        guard dict["postKey"] != nil else {return nil }
        guard dict["rate"] != nil else {return nil }
        
        
        self.refInCloud = snap.ref
        self.postKey = dict["postKey"] as! String
        self.rate = dict["rate"] as! String
    }
}
