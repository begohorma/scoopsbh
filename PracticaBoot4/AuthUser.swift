//
//  AuthUser.swift
//  ScoopsBH
//
//  Created by Begoña Hormaechea on 6/4/17.
//  Copyright © 2017 COM. All rights reserved.
//

import UIKit
import Firebase

class AuthUser: UIViewController {

//    TODO: penssar que opción usar
   //listener
//    var handle: FIRAuthStateDidChangeListenerHandle!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtAuthStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Analítica sobre las pantallas
        FIRAnalytics.setScreenName("AuthUser", screenClass: "Authors")
        
        // Do any additional setup after loading the view.
        //Mostrar estado usuario
        if let user = FIRAuth.auth()?.currentUser {
            self.txtAuthStatus.text = "Signed in as " + user.email!;
            self.txtAuthStatus.textColor = UIColor.green
        }
        else {
            self.txtAuthStatus.text = "Signed Out";
            self.txtAuthStatus.textColor = UIColor.gray
        }
        //añadir listener para identificar cuando se ha cambiado el estado
        //al completion se le pasa el auth y el usuario logeado
//        FIRAuth.auth()?.addStateDidChangeListener({ (auth, user) in
//            print("El mail del usuario logado es \(String(describing: user?.email))")
//        })
    }

    
    // Crear Usuario nuevo
    @IBAction func btnCreateUser(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "CreateUser", parameters: ["": "" as NSObject])
        
        //comprobar que el email y la pass tienen valor
        if let email:String = txtEmail.text,
            let pass:String = txtPassword.text {
            guard  email != "", pass != "" else {
                print("email and password are required")
                self.txtAuthStatus.text = "email and password are required"
                self.txtAuthStatus.textColor = UIColor.red
                return
            }
            //crear usuario
            FIRAuth.auth()?.createUser(withEmail: email, password: pass) { (user, error) in
                //Si error
                if let error = error  {
                    print("error:\(error.localizedDescription)")
                    self.txtAuthStatus.text = error.localizedDescription
                    self.txtAuthStatus.textColor = UIColor.red

                }
                if let user = user {
                    self.txtAuthStatus.text = "Signed in as " + user.email!;
                    self.txtAuthStatus.textColor = UIColor.green
                    
                    self.txtEmail.text = nil;
                    self.txtPassword.text = nil;
                    
                    // Ir a NewPostController
                    self.performSegue(withIdentifier: "addPostAferAuth", sender: self)
                }
            }
        }
    
    
    }
    
   
    
    @IBAction func btnLogIn(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "LogIn", parameters: ["": "" as NSObject])
        
        //comprobar que el email y la pass tienen valor
        if let email:String = txtEmail.text,
            let pass:String = txtPassword.text {
            guard  email != "", pass != "" else {
                print("email and password are required")
                self.txtAuthStatus.text = "email and password are required"
                self.txtAuthStatus.textColor = UIColor.red
                return
            }
            //logearse con usuario
            FIRAuth.auth()?.signIn(withEmail: email, password: pass) { (user, error) in
                //Si error
                if let error = error  {
                    print("error:\(error.localizedDescription)")
                    self.txtAuthStatus.text = error.localizedDescription
                    self.txtAuthStatus.textColor = UIColor.red
                    
                }
                //mostrar usuario logado
                if let user = user {
                    self.txtAuthStatus.text = "Signed in as " + user.email!;
                    self.txtAuthStatus.textColor = UIColor.green
                    
                    self.txtEmail.text = nil;
                    self.txtPassword.text = nil;
                    
                    // Ir a AuthorPostList
                    self.performSegue(withIdentifier: "addPostAferAuth", sender: self)

                }
            }
        }

    }
    
    
    @IBAction func btnLogOut(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "Logout", parameters: ["screen": "AuthUser" as NSObject])
        makeLogout()
    }
    
    public func makeLogout(){
        //si usuario está logado
        if let _ = FIRAuth.auth()?.currentUser{
            do{
                try FIRAuth.auth()?.signOut()
                
            }catch let error{
                print (error)
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
