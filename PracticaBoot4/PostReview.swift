//
//  PostReview.swift
//  PracticaBoot4
//
//  Created by Juan Antonio Martin Noguera on 23/03/2017.
//  Copyright © 2017 COM. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class PostReview: UIViewController {
    
    var post:Post!
    var storageRef: FIRStorageReference!

    @IBOutlet weak var rateSlider: UISlider!
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var postTxt: UITextField!
    @IBOutlet weak var titleTxt: UITextField!
    
    @IBOutlet weak var rateSliderValue: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Analítica sobre las pantallas
        FIRAnalytics.setScreenName("PostReviewController", screenClass: "AllUser")
        
        //asignar valores a los elementos de la vista detalle
        titleTxt.text = post.title
        postTxt.text = post.text
        
        
        
        if post.photoUrl != ""{
            storageRef = FIRStorage.storage().reference(forURL: "gs://scoopsbh-61cb0.appspot.com/>")
        
      storageRef.child(post.photoUrl).data(withMaxSize: INT64_MAX, completion: { (data, error) in
            if let error = error {
                print("Error downloading: \(error.localizedDescription)")
                return
            }
    
            self.imagePost.image = UIImage.init(data: data!)
        })
        }else{
            self.imagePost.image = #imageLiteral(resourceName: "noPhptpAvailable.jpeg")
        }
        
        //aumentar el numero de lecturas y guardarlo en la BD
        
        post.readingNumber = post.readingNumber + 1
        //actualiza en posts
        post.refInCloud?.updateChildValues(["readingNumber": post.readingNumber])
        let num = post.readingNumber
        // actualizar en author-posts
        let authorsPostsRef = FIRDatabase.database().reference().child("author-posts")

            authorsPostsRef.child(post.author).queryOrderedByKey().queryEqual(toValue: post.refInCloud?.key).observeSingleEvent(of: .value, with: { (snap) in
                    for postsFB in snap.children{
                        let post = Post(snap: postsFB as! FIRDataSnapshot)
                        if post != nil{
                            post!.refInCloud?.updateChildValues(["readingNumber": num])                        }
                        
                    }
                })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func rateAction(_ sender: Any) {
        print("\((sender as! UISlider).value)")
        self.rateSliderValue.text = String((sender as! UISlider).value)
    }

    @IBAction func ratePost(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "RatePost", parameters: nil)
        // guardar un nuevo registro posts-rates
        
        let postsRatesRef = FIRDatabase.database().reference().child("posts-rates")
        
        //Post para subir a la BD
        let key = postsRatesRef.childByAutoId().key
        
        let postRate = ["postKey": post.refInCloud!.key,
                    "rate":self.rateSliderValue.text ?? 0] as [String : Any]
        
        let postRateRecord = ["\(key)":postRate]
        postsRatesRef.updateChildValues(postRateRecord)
        
        // consultar el número de valoraciones del post
        //actualizar en posts
        var sumRates:Double = 0
        var ratesNum = 0
        postsRatesRef.queryOrdered(byChild: "postKey").queryEqual(toValue: post.refInCloud!.key).observeSingleEvent(of: .value, with: { (snap) in
            ratesNum = Int(snap.childrenCount)
            
            for postsFB in snap.children{
                let postRate = PostRate(snap: postsFB as! FIRDataSnapshot)
                if postRate != nil{
                    sumRates = sumRates + Double(postRate!.rate)!
                }
                
            }
            //calcular la valoración media
            let av = sumRates / Double(ratesNum)
            
             //actualizar valoración media en post y auhor-posts
            
            //actualiza en posts
            self.post.refInCloud?.updateChildValues(["rateAv": av])
            
            let authorPostsRef = FIRDatabase.database().reference().child("author-posts")
            authorPostsRef.child(self.post.author).queryOrderedByKey().queryEqual(toValue: self.post.refInCloud?.key).observeSingleEvent(of: .value, with: { (snap) in
                for postsFB in snap.children{
                    let post = Post(snap: postsFB as! FIRDataSnapshot)
                    if post != nil{
                        post!.refInCloud?.updateChildValues(["rateAv": av])                        }
                    
                }
                 self.showOKAlert(title: "Rating Post", message: "Rate has been saved successfully")
              })
            
            })
           
    
        
        
       
        
        
    }
    
    public func showOKAlert(title: String, message:String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let deafaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(deafaultAction)
            self.present(alert,animated: true,completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
