//
//  MainTimeLine.swift
//  PracticaBoot4
//
//  Created by Juan Antonio Martin Noguera on 23/03/2017.
//  Copyright © 2017 COM. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class MainTimeLine: UITableViewController {

    var model: [Post] = []
    let cellIdentier = "POSTSCELL"
    var postsRef: FIRDatabaseReference!
    var storageRef: FIRStorageReference!
    
    
    @IBAction func btnLogOut(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "Logout", parameters: ["screen": "MainTimeLine" as NSObject])
    
        let au = AuthUser()
        au.makeLogout()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.pullModel()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        //Analítica sobre las pantallas
        FIRAnalytics.setScreenName("MainTimeLine", screenClass: "Main")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.refreshControl?.addTarget(self, action: #selector(hadleRefresh(_:)), for: UIControlEvents.valueChanged)

    }
    
    func pullModel(){
        //referencia a la base de datos
        postsRef = FIRDatabase.database().reference().child("posts")
        
        //cargar los posts
        
       // let st = "ReadyToPublish"
        //let st = "NotPublished"
        let st = "Published" 
        
        self.model.removeAll()
        postsRef.queryOrdered(byChild:"status").queryEqual(toValue: st)
            .observe(.value, with: {(snap)in
                
                print(snap.childrenCount)
                for postsFB in snap.children{
                    let post = Post(snap: postsFB as! FIRDataSnapshot)
                    if post != nil{
                        self.model.append(post!)
                    }
                    
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }){(error) in
                print(error.localizedDescription)
        }

    }

    
    
    func hadleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentier, for: indexPath)
       
        if self.model.count > 0 {
            let num: String = String(model[indexPath.row].readingNumber)
            let av: String = String(model[indexPath.row].rateAv)

//            cell.textLabel?.text = model[indexPath.row].title + " - " + model[indexPath.row].status + "-" + num + "-" + av
            
             cell.textLabel?.text = model[indexPath.row].title
             cell.detailTextLabel?.text = "Read: " + num + "- Rate Av: " + av
            
            if model[indexPath.row].photoUrl != ""{
                storageRef = FIRStorage.storage().reference(forURL: "gs://scoopsbh-61cb0.appspot.com/>")
                
                storageRef.child(model[indexPath.row].photoUrl).data(withMaxSize: INT64_MAX, completion: { (data, error) in
                    if let error = error {
                        print("Error downloading: \(error.localizedDescription)")
                        return
                    }
                    cell.imageView?.image = UIImage.init(data: data!)
                })
            }else{
                cell.imageView?.image = #imageLiteral(resourceName: "noPhptpAvailable.jpeg")
            }
            
        }else
        {
           cell.textLabel?.text = ""
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "ShowRatingPost", sender: indexPath)
    }


    // MARK: - Navigation
 
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowRatingPost" {
            //analitics
            FIRAnalytics.logEvent(withName: "ReadPost", parameters:nil)
            let vc = segue.destination as! PostReview
            
            // aqui pasamos el item selecionado
//           let i = tableView.indexPathForSelectedRow?.row
            vc.post = model[tableView.indexPathForSelectedRow!.row]
            
        }
        if segue.identifier == "authenticateUser"{
           
//            if loginSuccess == true{
            //si usuario logeado ir a AuthorPostsList
            if (FIRAuth.auth()?.currentUser) != nil {
                performSegue(withIdentifier: "addPost", sender: self)
            }
            print("autenticar usuario")
        }
        if segue.identifier == "addPost"{
            print("nuevo post")
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
            if identifier == "authenticateUser" {
//                Si el usuario está logeado cortar el segue y cambiarlo por el que va al AuthorPost
                if (FIRAuth.auth()?.currentUser) != nil{
                     performSegue(withIdentifier: "addPost", sender: self)
                    return false
                }
            }
       
        return true
    }

    
    
        
    

}
