//
//  NewPostController.swift
//  PracticaBoot4
//
//  Created by Juan Antonio Martin Noguera on 23/03/2017.
//  Copyright © 2017 COM. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class NewPostController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var titlePostTxt: UITextField!
    @IBOutlet weak var textPostTxt: UITextField!
    @IBOutlet weak var imagePost: UIImageView!
    
    var isReadyToPublish: Bool = false
    var imageCaptured: UIImage! {
        didSet {
            imagePost.image = imageCaptured
        }
    }
    
    var storageRef: FIRStorageReference!
    
    //referencia a la base de datos
    let postsRef = FIRDatabase.database().reference().child("posts")
    let authorPostsRef = FIRDatabase.database().reference().child("author-posts")
    let userID = FIRAuth.auth()?.currentUser?.uid
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //Analítica sobre las pantallas
        FIRAnalytics.setScreenName("NewPostController", screenClass: "Authors")
        storageRef = FIRStorage.storage().reference(forURL: "gs://scoopsbh-61cb0.appspot.com/>")


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        self.present(pushAlertCameraLibrary(), animated: true, completion: nil)
    }
    @IBAction func publishAction(_ sender: Any) {
        isReadyToPublish = (sender as! UISwitch).isOn
    }

    @IBAction func savePostInCloud(_ sender: Any) {
        // preparado para implementar codigo que persita en el cloud 
        guard titlePostTxt.text != "" else {
            print("Title is mandatory")
            showOKAlert(title: "Adding New Post", message: "Title is Mandatory")
            return
        }
        guard textPostTxt.text != "" else {
            print("Content is mandatory")
            showOKAlert(title: "Adding New Post", message: "Content is Mandatory")
            return
        }
        if imagePost.image == nil{
            addNewPost(title: titlePostTxt.text!, text: textPostTxt.text!, userId: userID!,imgData:nil) { (error) in
                
                if let error = error{
                    print(error.localizedDescription)
                    self.showOKAlert(title: " Error Adding New Post", message: error.localizedDescription)
                    return
                    
                }
                print("sucess!")
                self.showOKAlert(title: "Adding New post", message: "Succes!")
                
                //Si vuelvo al anterior automáticamente casca al cargar los datos
                //            //volver a vc anterior
                //            _ = self.navigationController?.popViewController(animated: true)
                
            }
            
        }else{
            addNewPost(title: titlePostTxt.text!, text: textPostTxt.text!, userId: userID!,imgData:UIImageJPEGRepresentation((imagePost?.image)!, 0.5)) { (error) in
                
                if let error = error{
                    print(error.localizedDescription)
                    self.showOKAlert(title: " Error Adding New Post", message: error.localizedDescription)
                    return
                    
                }
                print("sucess!")
                self.showOKAlert(title: "Adding New post", message: "Succes!")
                
                //Si vuelvo al anterior automáticamente casca al cargar los datos
                //            //volver a vc anterior
                //            _ = self.navigationController?.popViewController(animated: true)
                
            }
        }
        


        }
    
    
    // MARK: -  Alerts
    
    public func showOKAlert(title: String, message:String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let deafaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(deafaultAction)
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    // MARK: -  Firebase Database
    

    func addNewPost(title:String, text:String,userId:String,imgData:Data!=nil,completion:@escaping ((Error?)->Void)){    //id post
        let key = postsRef.childByAutoId().key
        
        var status = "NotPublished"
        if isReadyToPublish{
           status = "ReadyToPublish"
        }
        
        var photoUrl = ""

        if (imgData) != nil{
            //el nombre de la imagen asociada al post será la clave del post.
            photoUrl = "images/\(key).jpg"
            
            //subir la photo y cuando esté subida se guardan los datos en la BD
            // crear metadatos imagen
            let metadata = FIRStorageMetadata()
            metadata.contentType = "image/jpeg"
            
            // subir imagen y metadatos
            
            storageRef.child(photoUrl).put(imgData, metadata: metadata) { (metadata, error) in
                if let error = error{
                    completion(error)
                    return
                    
                }
                if error == nil{
                    //upload success
                    //Post para subir a la BD
                    let post = ["title": title,
                                "text": text,
                                "photoUrl":photoUrl,
                                "author": userId,
                                "status": status,
                                "readingNumber": 0,
                                "rateAv":0] as [String : Any]
                    
                    let postRecord = ["\(key)":post]
                    let authorPostsRecords = ["\(String(describing: userId))/\(key)":post]
                    
                    self.postsRef.updateChildValues(postRecord)
                    self.authorPostsRef.updateChildValues(authorPostsRecords)
                    completion(nil)
                }
            }
        }else{
            //Post para subir a la BD
            let post = ["title": title,
                        "text": text,
                        "photoUrl":photoUrl,
                        "author": userId,
                        "status": status,
                        "readingNumber": 0,
                        "rateAv":0] as [String : Any]
            
            let postRecord = ["\(key)":post]
            let authorPostsRecords = ["\(String(describing: userId))/\(key)":post]
            
            self.postsRef.updateChildValues(postRecord)
            self.authorPostsRef.updateChildValues(authorPostsRecords)
            completion(nil)
        }
        

        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - funciones para la camara
    internal func pushAlertCameraLibrary() -> UIAlertController {
        let actionSheet = UIAlertController(title: NSLocalizedString("Selecciona la fuente de la imagen", comment: ""), message: NSLocalizedString("", comment: ""), preferredStyle: .actionSheet)
        
        let libraryBtn = UIAlertAction(title: NSLocalizedString("Usar la libreria", comment: ""), style: .default) { (action) in
            self.takePictureFromCameraOrLibrary(.photoLibrary)
            
        }
        let cameraBtn = UIAlertAction(title: NSLocalizedString("Usar la camara", comment: ""), style: .default) { (action) in
            self.takePictureFromCameraOrLibrary(.camera)
            
        }
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        actionSheet.addAction(libraryBtn)
        actionSheet.addAction(cameraBtn)
        actionSheet.addAction(cancel)
        
        return actionSheet
    }
    
    internal func takePictureFromCameraOrLibrary(_ source: UIImagePickerControllerSourceType) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        switch source {
        case .camera:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                picker.sourceType = UIImagePickerControllerSourceType.camera
            } else {
                return
            }
        case .photoLibrary:
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        case .savedPhotosAlbum:
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        
        self.present(picker, animated: true, completion: nil)
    }

}

// MARK: - Delegado del imagepicker
extension NewPostController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageCaptured = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        self.dismiss(animated: false, completion: {
        })
    }
    
}












