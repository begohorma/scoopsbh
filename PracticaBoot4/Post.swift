//
//  Post.swift
//  ScoopsBH
//
//  Created by Begoña Hormaechea on 7/4/17.
//  Copyright © 2017 COM. All rights reserved.
//

import Foundation
import Firebase

class Post: NSObject{
    
    var title: String
    var text: String
    var photoUrl: String
//    var location: Dictionary<String,String>
    var author: String // usuario logado
    var status: String //NotPublished | ReadyToPublish | Published
    var readingNumber: Int
    var rateAv: Double
    var refInCloud: FIRDatabaseReference?
    
    
    init(title:String, text:String,photoUrl:String,author:String,status:String) {
        self.title = title
        self.text = text
        self.photoUrl = photoUrl
        self.author = author
        self.status = status
        self.readingNumber = 0
        self.rateAv = 0
        self.refInCloud = nil
    }
    
    //init basado en el snapshot
    init?(snap: FIRDataSnapshot){
        guard let dict = snap.value as? [String:Any] else{ return nil}
        guard dict["title"] != nil else {return nil }
        guard dict["text"] != nil else {return nil }
        guard dict["photoUrl"] != nil else {return nil }
        guard dict["author"] != nil else {return nil }
        guard dict["status"] != nil else {return nil }
        guard dict["readingNumber"] != nil else {return nil }
        guard dict["rateAv"] != nil else {return nil }
        
        self.refInCloud = snap.ref
        self.title = dict["title"] as! String
        self.text = dict["text"] as! String
        self.photoUrl = dict["photoUrl"] as! String
        self.author = dict["author"] as! String
        self.status = dict["status"] as! String
        self.readingNumber = dict["readingNumber"] as! Int
        let ra:Double = Double(dict["rateAv"] as! NSNumber)//????
        self.rateAv = ra
    
    }
    
    //init de conveniencia
    convenience override init(){
        self.init(title:"", text:"",photoUrl:"",author:"",status:"")
    }

}
