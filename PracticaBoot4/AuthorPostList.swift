//
//  AuthorPostList.swift
//  PracticaBoot4
//
//  Created by Juan Antonio Martin Noguera on 23/03/2017.
//  Copyright © 2017 COM. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage


class AuthorPostList: UITableViewController {

    let cellIdentifier = "POSTAUTOR"
    
    var postsRef: FIRDatabaseReference!
    var storageRef: FIRStorageReference!
    
    var model: [Post] = []
    
    
    
    @IBAction func btnLogOut(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "Logout", parameters: ["screen": "AuthorPostList" as NSObject])
        
        let au = AuthUser()
        au.makeLogout()
        
        //volver a pantalla principal para usuarios no logados
        self.performSegue(withIdentifier: "backAfterLogOut", sender: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = FIRAuth.auth()?.currentUser?.email
        
          self.pullModel()
        
     
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Analítica sobre las pantallas
        FIRAnalytics.setScreenName("AuthorPostListController", screenClass: "Authors")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.refreshControl?.addTarget(self, action: #selector(hadleRefresh(_:)), for: UIControlEvents.valueChanged)


    }
    
    func pullModel(){
        //referencia a la base de datos
        postsRef = FIRDatabase.database().reference().child("posts")
        let authorsPostsRef = FIRDatabase.database().reference().child("author-posts")
        let userID = FIRAuth.auth()?.currentUser?.uid
        //cargar los posts
        self.model.removeAll()
        //        authorsPostsRef.child(userID!).observe(.value, with: {(snap)in
        authorsPostsRef.child(userID!).observeSingleEvent(of:.value, with: {(snap)in
            for postsFB in snap.children{
                let post = Post(snap: postsFB as! FIRDataSnapshot)
                if post != nil{
                    self.model.append(post!)
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }){(error) in
            print(error.localizedDescription)
        }
    }
    
    func hadleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let value = model[indexPath.row].title + " -  Status: " + model[indexPath.row].status
        
//            + " - RateAv: " + String(model[indexPath.row].rateAv) + " - Num.Read:" + String(model[indexPath.row].readingNumber)
//            + " - " + model[indexPath.row].photoUrl
        
        cell.detailTextLabel?.text = " RateAv: " + String(model[indexPath.row].rateAv) + " Num.Read: " + String(model[indexPath.row].readingNumber)
        cell.textLabel?.text = value
        if model[indexPath.row].photoUrl != ""{
            storageRef = FIRStorage.storage().reference(forURL: "gs://scoopsbh-61cb0.appspot.com/>")
            
            storageRef.child(model[indexPath.row].photoUrl).data(withMaxSize: INT64_MAX, completion: { (data, error) in
                if let error = error {
                    print("Error downloading: \(error.localizedDescription)")
                    return
                }
                cell.imageView?.image = UIImage.init(data: data!)
            })
        }else{
            cell.imageView?.image = #imageLiteral(resourceName: "noPhptpAvailable.jpeg")
        }

    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //TODO - Hacer publicacion y sobre todo borrado de forma transacional para que no se quede inestable si se produce un fallo.
        
        let publish = UITableViewRowAction(style: .normal, title: "Publish") { (action, indexPath) in
            
            //analitics
            FIRAnalytics.logEvent(withName: "Publish", parameters: nil)
            // Codigo para publicar el post
            let postToPublish = self.model[indexPath.row]
            if postToPublish.status == "NotPublished"
            {
                postToPublish.status = "ReadyToPublish"
            }
            
            //actualizar en author-post
            postToPublish.refInCloud?.updateChildValues(["status": postToPublish.status])
            
            //actualizar en posts
            self.postsRef.queryOrderedByKey().queryEqual(toValue: postToPublish.refInCloud?.key).observeSingleEvent(of: .value, with: { (snap) in
                for postsFB in snap.children{
                    let post = Post(snap: postsFB as! FIRDataSnapshot)
                    if post != nil{
                        post?.status = "ReadyToPublish"
                        post?.refInCloud?.updateChildValues(["status": post!.status])
                    }
                    
                }
            })
        
            //Actualizar los cambios en la vista
            self.pullModel()
            
        }
        publish.backgroundColor = UIColor.green
        let deleteRow = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            //analitics
            FIRAnalytics.logEvent(withName: "Delete", parameters: nil)
            // codigo para eliminar
            let postToDelete = self.model[indexPath.row]
            
            //eliminar en posts
            self.postsRef.queryOrderedByKey().queryEqual(toValue: postToDelete.refInCloud?.key).observeSingleEvent(of: .value, with: { (snap) in
                for postsFB in snap.children{
                    let post = Post(snap: postsFB as! FIRDataSnapshot)
                    if post != nil{
                       post?.refInCloud?.removeValue()
                    }
                    
                }
            })
            
            //eliminar la imagen en el storage
            if postToDelete.photoUrl != ""{
               self.storageRef = FIRStorage.storage().reference(forURL: "gs://scoopsbh-61cb0.appspot.com/>")
                self.storageRef.child(postToDelete.photoUrl).delete(completion: { (error) in
                    if let error = error{
                        print(error.localizedDescription)
                    }
                })
            }
            
            //elimar el post de author-posts
            postToDelete.refInCloud?.removeValue()
            
            
            
            self.pullModel()
            
            
          
        }
        return [publish, deleteRow]
    }

  // MARK: -  Publish
    
    
    @IBAction func publisReadyPosts(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "Publish Ready Posts", parameters: nil)
        
        // modificar es estado de los post de autor que estén en ReadyToPublish a Published
        //referencia a la base de datos
        postsRef = FIRDatabase.database().reference().child("posts")
        let authorsPostsRef = FIRDatabase.database().reference().child("author-posts")
        let userID = FIRAuth.auth()?.currentUser?.uid
        //cargar los posts
        self.model.removeAll()
        
        //Actualizar en author-posts
        authorsPostsRef.child(userID!).queryOrdered(byChild:"status").queryEqual(toValue: "ReadyToPublish").observeSingleEvent(of:.value, with: {(snap)in
            for postsFB in snap.children{
                let auPost = Post(snap: postsFB as! FIRDataSnapshot)
                if auPost != nil{
                    auPost?.status = "Published"
                    auPost?.refInCloud?.updateChildValues(["status": auPost!.status])
                }
                
                //actualizar en posts
                self.postsRef.queryOrderedByKey().queryEqual(toValue:auPost!.refInCloud?.key).observeSingleEvent(of: .value, with: { (snap) in
                    for postsFB in snap.children{
                        let post = Post(snap: postsFB as! FIRDataSnapshot)
                        if post != nil{
                            post?.status = "Published"
                            post?.refInCloud?.updateChildValues(["status": post!.status])
                        }
                        
                    }
                })
            }
            
            self.pullModel()

        }){(error) in
            print(error.localizedDescription)
        }

        
    }
    
    //TODO: Estos métodos son iguales Menos la query. Refactorizar para hacer un sólo metodo al que se le pase la query o el posible valor del estado
    
    @IBAction func showPublishedPosts(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "showPublishedPosts", parameters: nil)
        
        //referencia a la base de datos
        postsRef = FIRDatabase.database().reference().child("posts")
        let authorsPostsRef = FIRDatabase.database().reference().child("author-posts")
        let userID = FIRAuth.auth()?.currentUser?.uid
        //cargar los posts
        self.model.removeAll()
        authorsPostsRef.child(userID!).queryOrdered(byChild:"status").queryEqual(toValue: "Published").observeSingleEvent(of:.value, with: {(snap)in
            for postsFB in snap.children{
                let post = Post(snap: postsFB as! FIRDataSnapshot)
                if post != nil{
                    self.model.append(post!)
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }){(error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func showNotReadyToPublishPosts(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "showNotReadyToPublishPosts", parameters: nil)
        //analitics
        FIRAnalytics.logEvent(withName: "showPublishedPosts", parameters: nil)
        
        //referencia a la base de datos
        postsRef = FIRDatabase.database().reference().child("posts")
        let authorsPostsRef = FIRDatabase.database().reference().child("author-posts")
        let userID = FIRAuth.auth()?.currentUser?.uid
        //cargar los posts
        self.model.removeAll()
        authorsPostsRef.child(userID!).queryOrdered(byChild:"status").queryEqual(toValue: "NotPublished").observeSingleEvent(of:.value, with: {(snap)in
            for postsFB in snap.children{
                let post = Post(snap: postsFB as! FIRDataSnapshot)
                if post != nil{
                    self.model.append(post!)
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }){(error) in
            print(error.localizedDescription)
        }

    }
    
    @IBAction func showReadyToPostPosts(_ sender: Any) {
        //analitics
        FIRAnalytics.logEvent(withName: "showReadyToPostPosts", parameters: nil)
        //analitics
        FIRAnalytics.logEvent(withName: "showPublishedPosts", parameters: nil)
        
        //referencia a la base de datos
        postsRef = FIRDatabase.database().reference().child("posts")
        let authorsPostsRef = FIRDatabase.database().reference().child("author-posts")
        let userID = FIRAuth.auth()?.currentUser?.uid
        //cargar los posts
        self.model.removeAll()
        authorsPostsRef.child(userID!).queryOrdered(byChild:"status").queryEqual(toValue: "ReadyToPublish").observeSingleEvent(of:.value, with: {(snap)in
            for postsFB in snap.children{
                let post = Post(snap: postsFB as! FIRDataSnapshot)
                if post != nil{
                    self.model.append(post!)
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }){(error) in
            print(error.localizedDescription)
        }

    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
//     MARK: - Navigation

//     In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    }
    

}
